## Utilisation du Kanban pour le suivi de projet

Le Kanban c'est le seul endroit où on peut savoir où on en est. 
En tout temps.  

**Les développeurs [Dev] et [ChfDev]** le font vivre au fur et à mesure du développement.
Le moment précis du mouvement d'une colonne à une autre apparaît dans la documentation côté développeurs : ["Développer en collaboration sur un projet git avec GitLab et RStudio"](#collaborer-git).
On peut toutefois résumer l'utilisation du Kanban ainsi :

- [**Bloqué**]{style="color: #7F8C8D"} : Les développeurs y déplacent les tickets qui nécessitent des informations complémentaires de l'équipe éditoriale [Ed] pour être traités. On y trouve donc tout ce qui ne peut pas être réglé entre [Dev].
- [**En cours**]{style="color: #428BCA;"} : Les développeurs l'utilisent pour indiquer la prise en charge d'une fonctionnalité. Les développeurs qui prennent un ticket le déplace de la colonne [**Prêt**]{style="color: #69D100"} vers cette colonne [**En cours**]{style="color: #428BCA;"}.
- [**Révision**]{style="color: #F0AD4E"} : Les développeurs qui ont résolu un ticket ouvrent une _Merge Request_ (MR) pour fusion de leur branche vers la branche principale de développement : _dev_. Les [ChfDev] révisent la _Merge Request_ et décident ou non de l'accepter.
- [**A valider**]{style="color: #D10069"} : Lorsque la _MR_ est acceptée, la balle est renvoyée dans le camp des éditos [Ed]. Les [ChfDev] glissent le ticket dans cette colonne. Ils indiquent clairement aux [Ed] comment valider le contenu du ticket, où trouver les sites web, les lignes de commandes, ... pour valider dans de bonnes conditions.
- **Closed** : Une fois le ticket validé, les _commit_ peuvent être intégrés à la branche stable : _production_. Le ticket est fermé.

**Le comité éditorial [Ed]** viendra régulièrement voir ce qu'il se passe dans le Kanban.
Ils vérifieront en particulier que tout est pris en charge dans la partie [**Bloqué**]{style="color: #7F8C8D"} et en cours de résolution.
Ils regarderont attentivement les tickets de la colonne [**A valider**]{style="color: #D10069"} pour voir s'il n'y a pas des tickets qui peuvent être validés en asynchrone plutôt que d'attendre la prochaine réunion.


## Utilisation du Kanban pour la communication

Lorsque les développeurs prennent en charge un ticket, il est déplacé dans la colonne [**En cours**]{style="color: #428BCA;"}.
**L'ensemble des discussions relevant de ce ticket doit être consigné en tant que commentaire dans ce ticket**.
Si des discussions se sont déroulées ailleurs (Chat, réunions, ...), la partie du compte-rendu en lien avec un ticket doit être copié dans un commentaire du ticket en question.  

Si vous souhaitez un retour d'une personne en particulier, notez que vous pouvez nommer n'importe quel utilisateur (ayant accès au dépôt) dans le commentaire d'un ticket. 
De cette manière, vous pouvez interagir en asynchrone sur le développement de fonctionnalités. 
La personne nommée reçoit un email avec le contenu du commentaire et uniquement le contenu de ce commentaire.
Ainsi, si vous nommez quelqu'un dans un ticket pour susciter une réaction, posez une question claire dans ce commentaire et qui ne nécessite pas de relire la totalité des messages du ticket pour comprendre ce dont il s'agit.
En particulier, lors de l'étape de validation d'un ticket.
Veillez aussi à rester parcimonieux dans cette pratique pour ne pas encombrer les boîtes mails plus que de raison. 
Notamment parce que la totalité des commentaires qui seront ensuite ajoutés à ce ticket sera envoyée à toutes les personnes nommées.
Par ailleurs, vous pouvez faire une annonce / demande de volontaires sur le [logiciel de Chat](#outils-et-moyens-de-communiquer) au préalable si vous ne savez pas précisément à qui poser votre question.

> Privilégier les interactions en asynchrone. Cela permet à chacun de définir son emploi du temps et de répondre quand il peut. Vous trouverez toujours autre chose à faire d'aussi intéressant en attendant une réponse. De la documentation par exemple !



