# [dev] - Créer des tickets et gérer la communication et l'avancement avec un Kanban {#tickets-kanban}

La gestion de projet s'effectue grâce à un Kanban sur GitLab.
Le Kanban est mis en place par les [ChfDev] lors de l'étape ["Mettre en place un projet de développement avec RStudio et GitLab"](#dev-gerer-gitlab).
Chaque [dev] se réfère à tout moment au contenu du Kanban pour **savoir quelles fonctionnalités / bugs sont à prendre en charge en priorité**.
Chaque [dev] **indique l'avancée de son travail** dans le Kanban et dans les tickets associés.
Les discussions avec les membres du comité éditorial [Red/Rel] se passe dans les tickets pour **assurer le référencement de chaque décision** à l'endroit approprié.

<!-- ## Présentation du Kanban et des labels -->
```{r, echo=FALSE, child="sections/kanban.Rmd"}
```


## Transformer la liste des demandes du comité éditorial en tickets

Dans la phase ["Comment définir ses besoins et le contenu"](#lister-besoins), le comité éditorial doit avoir fourni la liste de ses besoins pour la publication.
Les fonctionnalités prioritaires doivent aussi être définies lors de cette phase. 
Les [ChfDev] divisent les fonctionnalités en tickets.
Il s'agit de **découper les demandes d'un point de vue technique**, en sous-tâches de développement.
Il n'est pas toujours évident de trouver le découpage adéquat entre diviser en tâches d'une heure ou en tâches de 15 jours.  

> Si vous ne savez pas par où commencer, créez des (méta-issues) pour chaque chapitre. Au minimum on s'assure que toutes les parties demandées sont bien référencées comme issue. Vous pourrez ensuite y référencer les sous-issues qui en découlent. 

Nous recommandons donc un découpage selon la structure de la production (par page, chapitre, section, onglet, ...). 
De cette manière, on pourra recoller les morceaux plus facilement lorsqu'ils auront été développés. 
Ce sera plus pratique pour le comité éditorial de suivre le fil des développements s'ils peuvent se raccrocher au découpage qu'ils ont mis tant de temps à définir.
Et ce sera plus intéressant pour les développeurs de partir sur des petites fonctionnalités dont le résultat est visible directement dans la composition finale.
Penser au maximum à la répartition de tâches qui peuvent se réaliser en parallèle de manière la plus indépendante possible.
Il s'agira ensuite de réfléchir aux fonctions qui seront équivalentes sur différentes parties pour noter les liens dans les différents tickets créés et éventuellement définir l'ordre d'exécution.

> Si vous avez suivi le conseil de noter le cahier des charges dans le Wiki, n'hésitez pas à y lister les liens vers les tickets créés. Ainsi, vous saurez ce que vous avez déjà pris en compte et le comité éditorial [Ed] pourra aussi plus facilement s'en assurer. Par ailleurs, s'il s'avère que le comité éditorial [Ed] décide de modifier des demandes en cours en route, ils sauront qu'ils ne peuvent pas le faire sans discussion préalable lorsqu'un ticket est déjà ouvert.

Dans le cadre d'un développement PROPRE, en suivant les recommandations de développement en mode _"Rmd first"_, pour l'écriture d'un chapitre vous aurez toujours à créer des vignettes pour chaque morceau de code.
Dans un premier temps, chaque développeur créera un morceau dans une vignette séparée, quitte à tout recoller à la fin.  

**À quoi faut-il penser lors du découpage ?**

- Indépendance de la résolution des tickets
- Étape de préparation des données. Probablement bloquant pour beaucoup de tickets suivant.
  + Cela inclut la création de petits jeux de données reproductibles qui seront utilisés lors du développement, des exemples de chaque fonction développées et de leurs tests. Ces mini-jeux de données resteront dans le package pour les tests et la documentation en intégration continue. Vous devrez donc définir en amont ce qui est diffusable ou non parmi vos données.
- Chaque partie contient potentiellement :
  + une extraction de données
  + un calcul d'indicateurs
  + l'affichage d'un tableau
  + l'affichage d'un graphique
  + l'affichage d'un phrase _"in-line"_ pour présenter en toutes lettres le résultat de calculs.
- Toutes ces étapes et fonctions peuvent être paramétrables selon la zone géographique ou la période sur laquelle on travaille.
- Séparer au maximum le fond de la forme. Par exemple, pour un graphique, faîtes d'abord valider l'information présentée et le type de graphique, avant de faire valider sa version avec le thème / l'apparence définitive. De même pour les tableaux.

## Prioriser et assigner les tickets

Par défaut tous les tickets sont listés sur le Kanban principal dans la première colonne (*open*).
Vous pouvez d'ores et déjà trier les tickets [**bloqués**]{style="color: #7F8C8D"}, ceux qui nécessitent des précisions supplémentaires par le comité éditorial.
Il s'agit des tickets qu'on ne peut pas résoudre sans information complémentaire.
Les tickets qui nécessitent la résolution d'un autre ticket au préalable ne sont pas à lister ici, mais l'information est consignée dans sa description.
Choisissez ensuite une dizaine de tickets à réaliser en priorité et les glisser vers la colonne [**Prêt**]{style="color: #69D100"}. 
Il ne faut pas en mettre de trop. 
Vous pouvez pré-trier les tickets qui arriveront après ceux qui sont prêts, en leur assignant l'étiquette [**En attente**]{style="color: #A8D695"}. 
Cela signifie que ce qu'il faut faire est clair pour les [ChfDev] et que le ticket attend son tour pour passer en [**Prêt**]{style="color: #69D100"}.
Notez que vous pouvez ré-ordonner les tickets dans chaque colonne, en mettant en haut de colonne les tickets avec une priorité plus élevée.

> Il est conseillé de ne pas avoir plus d'une dizaine de tickets dans les colonnes [**Prêt**]{style="color: #69D100"}, [**En cours**]{style="color: #428BCA;"} et [**Révision**]{style="color: #F0AD4E"}.

Définissez ensuite, en discussion avec les développeurs, qui est assigné à quel ticket. 
Les [ChfDev] listent sur le logiciel de Chat les tickets à prendre et peuvent éventuellement proposer des tickets directement à certains développeurs nommés.

> Notez que vous pouvez définir des _milestones_ ou points d'étapes dans le menu _Issues_ > _Milestones_. Ce sont des objectifs que vous fixez dans le temps, avec une date de livraison. Par exemple, la livraison du premier chapitre de la publication reproductible. Chaque ticket peut être assigné à un _milestone_. Cela vous permet d'organiser votre Kanban mais aussi de suivre l'avancement spécifique du _Milesones_ dans sa page de suivi.

<!-- ## Utilisation du Kanban pour le suivi de projet -->
<!-- ## Utilisation du Kanban pour la communication -->

```{r, echo=FALSE, child="sections/kanban-utilisation.Rmd"}
```
