# PROPRE

Ce dépôt contient un [**bookdown**](https://github.com/rstudio/bookdown) des bonnes pratiques de développement pour les projets de type PROPRE.

- Lire le livre numérique validé est à cette adresse : https://rdes_dreal.gitlab.io/publication_guide/production/

- Lire le livre numérique en développement est à cette adresse : https://rdes_dreal.gitlab.io/publication_guide/

## Qu'est-ce qu'un projet collaboratif PROPRE ?

Un projet collaboratif PROPRE est un projet qui suit le **"PROcessus de Publications REproductibles"** et qui est conduit par une équipe pluri-disciplinaire. 
Suite à la réponse de la DREAL Pays de la Loire à un appel à projet du Commissariat Général au Développement Durable (CGDD) fin 2015 sur le thème de la connaissance stratégique des territoires, un centre de service de la donnée a été constitué en 2018.
Le "DREAL datalab" doit **proposer des accès facilités aux données brutes, agrégées et analysées produites et les diffuser**.  
Afin de favoriser la réussite des projets, des méthodes modernes de développement et l'utilisation d'outils libres et open-source ont été testées. 
La production de données et d'analyses de ces données requièrent aujourd'hui leur **présentation dans un cadre reproductible** pour au moins trois raisons : la confiance en l’information, sa fraîcheur et le gain de temps à en produire des nouvelles. 
Ainsi s'est présentée la nécessité définir un PRocessus pour monter des projets collaboratifs de Publications REproductibles et donc diffusables. L'acronyme PROPRE est né.  
La mise en place de ce processus ne pouvait se réaliser que dans une seule équipe.
Une équipe pluri-disciplinaire d'agents de différentes DREAL s'est constituée pour mettre le processus à l'épreuve des besoins et contraintes de chacun sur un projet collaboratif commun. 
 
Les détails de la genèse et les concepts du processus se trouvent dans le livre numérique : [C’est du PROPRE !](https://rdes_dreal.gitlab.io/propre)  

## Que contient le présent livre numérique ?

Ce livre s'adresse aux différentes personnes impliquées dans un processus de développement PROPRE. Nous distinguerons trois catégories de contenu : 

- **[01 - coordination]** : Comment mettre en place un tel projet, quels sont les rôles à répartir, quels sont les moyens et les outils nécessaires, ... ?  
- **[02 - édition]** : Comment définir ses besoins, comment présenter le contenu des documents, comment collaborer avec l'équipe de développement, ... ?
- **[03 - développement]** : Comment lancer techniquement le projet, quels outils utiliser, quelles méthode de développement avec R, quelles sont les bonnes pratiques, ... ?

  
  
> Bien sûr, le contenu de ce document a été créé avec R et Markdown en suivant ce "PROcessus de Publications REproductibles" (PROPRE), documenté, testé, versionné avec une forge logicielle adaptée

