# [edition] Rédiger et fournir les textes en RMarkdown{#rediger-textes}

Les publications qui sortent du processus PROPRE contiennent des analyses de données, des tableaux et des figures.
Pour la lisibilité de ces publications, il est aussi nécessaire d'y inclure des titres, des chapîtres, des paragraphes de présentation, des phrases rédigées automatiquement selon les données qu'elles décrivent...
Ce sont les membres du comité éditorial [Ed] qui sont en charge de rédiger les textes à inclure dans la production.

## Stratégies d'échange de contenu entre [Dev] et [Ed]

```{r, echo=FALSE, child="sections/strategie-trame.Rmd"}
```

```{r, echo=FALSE, child="sections/langage-rmarkdown.Rmd"}
```

## Participer à l'ajout de textes sur git

Selon la stratégie employée, vous pouvez participer à l'ajout ou la modification de textes directement dans le projet, en utilisant _git_.
Pour cela, vous devrez suivre les instructions de ["Collaboration avec git et RStudio"](#collaborer-git).

